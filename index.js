var lightningHash = function (data) {
    return data + "*";
};
var Block = /** @class */ (function () {
    function Block(data, hash, lastHash) {
        this.data = data;
        this.hash = hash;
        this.lastHash = lastHash;
    }
    return Block;
}());
var BlockChain = /** @class */ (function () {
    function BlockChain() {
        var genesis = new Block('gen-data', 'gen-hash', 'gen-lastHash');
        this.chain = [genesis];
    }
    BlockChain.prototype.addBlock = function (data) {
        var lastHash = this.chain[this.chain.length - 1].hash;
        var hash = lightningHash(data + lastHash);
        var block = new Block(data, hash, lastHash);
        this.chain.push(block);
    };
    return BlockChain;
}());
var foodBlockhain = new BlockChain();
foodBlockhain.addBlock('one');
foodBlockhain.addBlock('two');
foodBlockhain.addBlock('three');
console.log(foodBlockhain);
